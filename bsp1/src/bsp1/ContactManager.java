package bsp1;


import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
 

public class ContactManager {
    public static void main(String[] args) {
        // loads configuration and creates a session 
        Configuration configuration = new Configuration().configure();
        ServiceRegistryBuilder registry = new ServiceRegistryBuilder();
        registry.applySettings(configuration.getProperties());
        ServiceRegistry serviceRegistry = registry.buildServiceRegistry();
        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
         
        // opens a new session 
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        
        // persists new Contact objects
        Contact contact1 = new Contact("Nam", "hainatuatgmail.com", "Vietnam", "0904277091");
        session.persist(contact1);
        Contact contact2 = new Contact("Bill", "billatgmail.com", "USA", "18001900");
        Serializable id = session.save(contact2);
        System.out.println("created id: " + id);
        
        // commits the transaction and closes the session
        session.getTransaction().commit();
        session.close();
    }
}
