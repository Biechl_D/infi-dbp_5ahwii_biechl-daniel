import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.jfree.ui.TextAnchor;

public class fluege{

	private static Connection c = null;
	public static java.sql.Statement stmt=null;
	private static PreparedStatement stm=null;
	
	private static ArrayList<Integer> seenTimeList = new ArrayList<Integer>();
	private static ArrayList<Integer> fluegeList = new ArrayList<Integer>();

	public static void main(String[] args) {

		String url = "jdbc:mysql://localhost/temperaturmessungen?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Berlin";
		//URL --> zeitzone wird auf Europa/Berlin gestellt da sonst Timezone Fehler und deaktiviert SSL(=verschlüsselte Verbindung zu MysqlServer)
		
		String user = "root";
		
		try
		{
			
			Class.forName("com.mysql.cj.jdbc.Driver");

			// mySQL-driver laden und Verbindung zur DB aufbauen
			System.out.println("Creating DBConnection"); 
			c = DriverManager.getConnection(url, user, ""); 
			
			stmt=c.createStatement();
			String database="USE aircraft";
			stmt.executeUpdate(database);
			System.out.println("successfull connection");
			//selectForDiagram();
			drawDiagram("Amount of Flights per Month");
			      
			      
			stmt.close();

		}
		catch(Exception e)
		{
			System.err.println("Couldn't create DBConnection"); 
			System.err.println(e.getClass().getName()+": "+ e.getMessage());
			System.exit(0);
		}
	}

//   public static void selectForDiagram() {
//	   
//	   
//		//String selectquery = "SELECT from_unixtime(seentime) AS newseentime, count(squawk) FROM dump1090data "
//		//		+ "GROUP BY MONTH(newseentime) where seentime BETWEEN <""> AND "">;";
//
//	   // %Y -> Jahr , %m -> Monat als Zahl
//		String selectquery = "SELECT FROM_UNIXTIME(seentime, '%Y'), FROM_UNIXTIME(seentime, '%m'), COUNT(DISTINCT flightnr) "
//				+ "FROM dump1090data GROUP BY MONTH(FROM_UNIXTIME(seentime)), YEAR(FROM_UNIXTIME(seentime)) "
//				+ "order by YEAR(FROM_UNIXTIME(seentime)), MONTH(FROM_UNIXTIME(seentime));";
//		try {
//			stm = c.prepareStatement(selectquery);
//			ResultSet rs = stm.executeQuery(selectquery);
//			//solange es ein nächstes Element in DB gibt wird while ausgeführt
//			while (rs.next()) {
//				//seenTimeList.add(rs.getInt(1));
//				//fluegeList.add(rs.getInt(2));
//				
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} 
//	}
   
   public static void drawDiagram(String chartTitle){

	   
	   JFreeChart barChart = ChartFactory.createBarChart(
			 chartTitle,
			 "Category",
			 "Score", 
			 createDataset(),
			 PlotOrientation.HORIZONTAL,           
	         false, true, false);
	         
	      ChartPanel chartPanel = new ChartPanel( barChart );        
	      chartPanel.setPreferredSize(new java.awt.Dimension( 560 , 367 ) ); 
	      
	     // CategoryItemRenderer renderer = ((CategoryPlot)barChart.getPlot()).getRenderer();
	      BarRenderer renderer = (BarRenderer) barChart.getCategoryPlot().getRenderer();

	      renderer.setItemMargin(-3); // Breite der Balken
	      //Zahlen auf Balken anzeigen
	      renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
	      renderer.setBaseItemLabelsVisible(true);
	      ItemLabelPosition position = new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_CENTER);
	      renderer.setBasePositiveItemLabelPosition(position);
	      
	      ApplicationFrame f1 = new ApplicationFrame("FluegeFrame");
	      f1.setContentPane( chartPanel );
	      f1.pack( );        
	      RefineryUtilities.centerFrameOnScreen(f1);        
	      f1.setVisible( true );
   }
   
   private static CategoryDataset createDataset( ) {
	      
	   final DefaultCategoryDataset dataset = new DefaultCategoryDataset( ); 
	   /*
	   final String fluege = "Fluege";
	    
	   for(int i=0;i< fluegeList.size();i++) {
		   System.out.println(fluegeList.get(i));
			//dataset.addValue(fluegeList.get(i),"Row "+i, seenTimeList.get(i));
		*/
	   
		// %Y -> Jahr , %m -> Monat als Zahl
			String selectquery = "SELECT FROM_UNIXTIME(seentime, '%Y'), FROM_UNIXTIME(seentime, '%m'), COUNT(DISTINCT flightnr) "
					+ "FROM dump1090data GROUP BY MONTH(FROM_UNIXTIME(seentime)), YEAR(FROM_UNIXTIME(seentime)) "
					+ "order by YEAR(FROM_UNIXTIME(seentime)), MONTH(FROM_UNIXTIME(seentime));";
			
			ArrayList<Integer> fluegeanzahl = new ArrayList<Integer>();
			ArrayList<String> months = new ArrayList<String>();
			
			try {	
				stm = c.prepareStatement(selectquery);
				ResultSet rs = stm.executeQuery(selectquery);
				//solange es ein nächstes Element in DB gibt wird while ausgeführt
				while (rs.next()) {
					
					fluegeanzahl.add(rs.getInt(3));
					int month = rs.getInt(2);
					int year = rs.getInt(1);
					if(month == 1) months.add("Jannuary "+ year);
					if(month == 2) months.add("February "+ year);
					if(month == 3) months.add("March "+ year);
					if(month == 4) months.add("April "+ year);
					if(month == 5) months.add("May "+ year);
					if(month == 6) months.add("June "+ year);
					if(month == 7) months.add("July "+ year);
					if(month == 8) months.add("August "+ year);
					if(month == 9) months.add("September "+ year);
					if(month == 10) months.add("October "+ year);
					if(month == 11) months.add("November "+ year);
					if(month == 12) months.add("December "+ year);
					
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}              
	   
			for(int i=0; i<=fluegeanzahl.size()-1; i++) {
				dataset.addValue(fluegeanzahl.get(i),"Row "+i, months.get(i));
			}
	      return dataset; 
	   }
}
