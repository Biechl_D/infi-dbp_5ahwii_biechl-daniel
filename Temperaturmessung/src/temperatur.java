import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;


public class temperatur{

	private static Connection c = null;
	public static java.sql.Statement stmt=null;
	private static PreparedStatement stm=null;
	
	int randTemp;
	private static Random r = new Random();
	static int Low = 1;
	static int High = 30;
	private static int vorherigeTemp = r.nextInt(High-Low) + Low; //erstellt Zufallszahl im Bereich 1-30
    
	private static Date date = new Date();
	
	static Scanner sc = new Scanner(System.in);
	static String information;
	
	private static ArrayList<Integer> list = new ArrayList<Integer>();
	private static ArrayList<String> listDate = new ArrayList<String>();

	public static void main(String[] args) {

		String url = "jdbc:mysql://localhost/temperaturmessungen?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Berlin";
		//URL --> zeitzone wird auf Europa/Berlin gestellt da sonst Timezone Fehler und deaktiviert SSL(=verschl�sselte Verbindung zu MysqlServer)
		
		String user = "root";
		
		try
		{
			
			Class.forName("com.mysql.cj.jdbc.Driver");

			// mySQL-driver laden und Verbindung zur DB aufbauen
			System.out.println("Creating DBConnection"); 
			c = DriverManager.getConnection(url, user, "pw"); 
			
			stmt=c.createStatement();
			String database="CREATE DATABASE IF NOT EXISTS temperaturmessungen;";
			stmt.executeUpdate(database);
			stmt.close();
			
			stmt=c.createStatement();
			database="USE temperaturmessungen";
			stmt.executeUpdate(database);
			
			stmt.close();

			createTableTemperatur();
	
			int x=0;
			while(true) 
			{
				System.out.println("To insert data type 1");
				System.out.println("For diagramm type 2");
				String s1 = sc.next();
				switch (s1) {
				case "1":
					if(x==0) {
						System.out.println("Press a for manuel input:");
						information = sc.nextLine();
						x++;
					}
					System.out.println(vorherigeTemp);
					System.out.println(getTime(date));
					randomTemp();
					insertIntoTable("temperature", vorherigeTemp, getTime(date));
					TimeUnit.MINUTES.sleep(10);   //alle 10Minuten insert ausf�hren
					break;
				case "2":
					selectForDiagram();
					//selectDateForDiagram();
					diagramTemp();
					break;
				default:
					System.out.println("Falsche Eingabe");
					break;
				}
			}
			// while --> sorgt f�r keinen Abbruch des Programms
	
		}
		catch(Exception e)
		{
			System.err.println("Couldn't create DBConnection"); 
			System.err.println(e.getClass().getName()+": "+ e.getMessage());
			System.exit(0);
		}
	}
	
	public static void createTableTemperatur() throws SQLException
	{
		stmt =  c.createStatement();

		String tableTemperaturen =
				"CREATE TABLE if not exists temperature "
						+ "(temp INT, "
						+ "datum DATETIME);";

		stmt.executeUpdate(tableTemperaturen);
		System.out.println("created table temperature successfully");
		stmt.close();
	}
	
	//methode f�r ZufallsTemperaturen
	public static void randomTemp() {
		
		int randTemp = vorherigeTemp + r.nextInt(3)-1;
		vorherigeTemp=randTemp;
	}
	
	//Datum-Zeit-methode
	public static String getTime(Date date) {
		
		//Zeit als Datetime 
		date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		//to convert Date to String, use format method of SimpleDateFormat class.
	    String currentTime = dateFormat.format(date);
		return currentTime;
	}
	
	//insert methode
	public static void insertIntoTable(String tablename, int temperatur, String datum) throws SQLException{
		try {
			String inserts=
					"INSERT INTO "
					+tablename
					+" VALUES(?,?)";
			
			
			stm = c.prepareStatement(inserts);
			
			if(information.equals("a")) {
				System.out.println("Type in Temperature:");
				String temp = sc.nextLine();
				int resultTemp = Integer.parseInt(temp);
				System.out.println("Type in date:");
				String date = sc.nextLine();
				
				stm.setInt(1, resultTemp);
			    stm.setString(2, date);
			}
			else{
				stm.setInt(1, temperatur);
			    stm.setString(2, datum);
			}
			
			stm.executeUpdate();
			System.out.println("inserted successfully");
			stm.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void selectForDiagram() {
		
		String selectquery = "SELECT * FROM temperature";

		try {
			stm = c.prepareStatement(selectquery);
			ResultSet rs = stm.executeQuery(selectquery);
			//solange es ein n�chstes Element in DB gibt wird while ausgef�hrt
			while (rs.next()) {
				list.add(rs.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}
	
public static void selectDateForDiagram() {
		
		String selectquery = "SELECT * FROM temperature";

		try {
			stm = c.prepareStatement(selectquery);
			ResultSet rs = stm.executeQuery(selectquery);
			//solange es ein n�chstes Element in DB gibt wird while ausgef�hrt
			while (rs.next()) {
				listDate.add(rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}
	
	public static void diagramTemp() {
		
		// Linie1 enthaelt Punkte, die verbunden werden
		//linke Seite = fortlaufende Nummer
		//rechte Seite == Temperaturen
		XYSeries series1 = new XYSeries("Punkte1");
		for(int i=0;i< list.size();i++) {
			series1.add(i, list.get(i));
		}
		
		/*TimeSeriesCollection seriesDate = new TimeSeriesCollection();
		for(int i=0;i< listDate.size();i++) {
			String dateFromList=listDate.get(i);
			SimpleDateFormat standardDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date myDate = standardDateFormat.parse(dateFromList);
			seriesDate.add(standardDateFormat);
		}*/
		//XYSeries seriesDate = new XYSeries("Punkte2");
		
		
		// Hinzufuegen von series1 und series2 zu der Datenmenge dataset
		XYSeriesCollection dataset2 = new XYSeriesCollection();
		dataset2.addSeries(series1);
		
		//create Plot using a Renderer
		XYSplineRenderer dot = new XYSplineRenderer();
		
		//Beschriftung der Achsen
		NumberAxis xax = new NumberAxis("Date");
		NumberAxis yax = new NumberAxis("Temperatur");
		
		//Skale Ax
		xax.setRange(-5, 40);
		yax.setRange(0, 40);
		
		//create Objekt 
		XYPlot plot = new XYPlot(dataset2,xax,yax, dot);
		
		//Object of class JFreeChart
		JFreeChart chart2 = new JFreeChart(plot);
		
		// Erstellen eines Ausgabefensters
		ApplicationFrame punkteframe = new ApplicationFrame("Punkte"); //"Punkte" entspricht der Ueberschrift des Fensters

		ChartPanel chartPanel2 = new ChartPanel(chart2);
		punkteframe.setContentPane(chartPanel2);
		punkteframe.pack();
		punkteframe.setVisible(true);
	}
}
